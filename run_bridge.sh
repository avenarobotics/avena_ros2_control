# setup
. /opt/ros/noetic/setup.bash && \
cd  ~/ros_ws && \
catkin_make_isolated --install

. /opt/ros/foxy/setup.bash && \
cd  ~/ros2_ws && \
colcon build --packages-select custom_interfaces

. /opt/ros/noetic/setup.bash && \
. /opt/ros/foxy/setup.bash && \
. ~/ros_ws/install_isolated/setup.bash && \
. ~/ros2_ws/install/local_setup.bash && \


colcon build --symlink-install --packages-select ros1_bridge --cmake-force-configure
. ~/ros2_ws/install/local_setup.bash 
ros2 run ros1_bridge dynamic_bridge --print-pairs | grep custom_interfaces
# OUTPUT
- 'custom_interfaces/srv/GetArmState' (ROS 2) <=> 'custom_msgs/GetArmState' (ROS 1)
- 'custom_interfaces/srv/SetArmTorques' (ROS 2) <=> 'custom_msgs/SetArmTorques' (ROS 1)
# Shell A
. /opt/ros/noetic/setup.bash
roscore

# core container 
./coppeliaSim.sh ../scenes/scenes/ava01_scene_brain_AvenaArmGripper.ttt -s
./coppeliaSim.sh ../scenes/scenes/for_Hassan.ttt -s
# Shell B
. /opt/ros/noetic/setup.bash
export ROS_MASTER_URI=http://localhost:11311
ros2 run ros1_bridge dynamic_bridge

# OUTPUT
Created 1 to 2 bridge for service /arm_controller/get_current_arm_state
Created 1 to 2 bridge for service /arm_controller/set_torques

# Shell C
. /opt/ros/noetic/setup.bash
source devel_isolated/setup.bash 
roslaunch rrbot_hw_interface rrbot_hardware.launch
roslaunch rrbot_hw_interface rrbot_test_trajectory.launch